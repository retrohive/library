#!/bin/sh
#
# Requires ImageMagick, unzip, and unrar

for b in "$@"
do
    start_time=`date +%s`
    echo "=== Processing $b ==="
    base=${b%%.*}
    ext=${b#*.}
    temporary=$(basename $0)
    TMPFILE=$(mktemp -d /tmp/${temporary}.XXXXXX) || exit 1
    # Convert CBZ or CBR as needed
    echo "Extracting pages..."
    if [ "$ext" == "cbr" ]; then
        unrar e -idc -idp -idq "$b" $TMPFILE/
    elif [ "$ext" == "cbz" ]; then
        unzip -q "$b" -d $TMPFILE/
    fi
    echo "Creating PDF..."
    convert $TMPFILE/*jpg "$base.pdf"
    echo "Created $base.pdf"
    echo "Cleaning up..."
    rm -rf $TMPFILE
    finish_time=`date +%s`
    min=`echo "scale=0; ($finish_time-$start_time)/60" | bc -l `
    sec=`echo "scale=0; ($finish_time-$start_time) % 60" | bc -l | awk '{printf "%02d", $0}'`
    echo Converted $b to $base.pdf in $min:$sec.
    echo ""
done
