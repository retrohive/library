set dotenv-load

clean:
	find "{{invocation_directory()}}" -not -name '*.md' -not -name 'PKGBUILD' -not -name 'cbztopdf.sh' -not -wholename './.git*' -not -wholename './import*' -not -wholename './todo*' -not -name 'Justfile' -not -name '.env' -not -name 'description.xml' -type f -print0 | xargs -0 rm -v

rsync FILE:
	rsync --rsync-path="sudo rsync" -avzr --inplace --progress "{{FILE}}" "$SSH_LOGIN@$SSH_HOST:$SSH_PATH"

deploy:
	find "{{invocation_directory()}}" -name '*.zst' -print0 | xargs --null -I {} just rsync "{}"
	ssh $SSH_LOGIN@$SSH_HOST "sudo ${SCRIPT}"
